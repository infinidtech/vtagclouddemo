﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace VTagCloudDemo.Libs
{
    public class CloudClient
    {
        private readonly string username, password;
        public const string CLOUDBASEURL = "https://vtagcloud.infinidtech.com/api/";
        public const string LOCALBASEURL = "http://localhost:5050/api/";
        private string BASEURL = CLOUDBASEURL;
        public enum Method { GET, PUT, POST, DELETE };
        protected AuthToken authToken;

        public CloudClient(string username, string password)
        {
            authToken = new AuthToken();
            this.username = username;
            this.password = password;
        }

        public void SetBaseUrl(string baseUrl)
        {
            BASEURL = baseUrl + "/api/";
        }

        public void SetUseCloud(bool useCloud)
        {
            if (useCloud)
                BASEURL = CLOUDBASEURL;
            else
                BASEURL = LOCALBASEURL;
        }     

        #region Http Call Helpers

        private async Task<HttpContent> GetHttpContent(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            if (TokenExpired() && !await GetToken())
            {
                throw new Exception("Error: Token invalid.");
            }
            try
            {
                using (var client = new HttpClient())
                {                    
                    client.BaseAddress = new Uri(BASEURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken.token);
                    HttpResponseMessage response = null;
                    string parms = String.Join("&", parameters.Select(p => p.Name + "=" + p.Value));
                    string reqUri = parms.Length > 0 ? resource + "/" + action + "?" + parms : resource + "/" + action;
                    string json = JsonConvert.SerializeObject(body);
                    switch (method)
                    {
                        case Method.GET:
                            response = await client.GetAsync(reqUri);
                            break;
                        case Method.POST:
                            response = await client.PostAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.PUT:
                            response = await client.PutAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.DELETE:
                            response = await client.DeleteAsync(reqUri);
                            break;
                    }
                    if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        return response.Content;
                    }
                    else
                    {
                        throw await HandleBadResponse(response, resource, action);
                    }
                }
            }
            catch (Exception exc)
            {
                string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                //_logger.LogError("Got Error:" + message);
                throw new Exception(message);
            }
        }

        protected async Task<S> DoHttp<S>(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, resource, action, body, parameters);
            string json = await content.ReadAsStringAsync();
            if (!string.IsNullOrWhiteSpace(json))
            {
                S result = JsonConvert.DeserializeObject<S>(json);
                return result;
            }
            return default(S);
        }

        protected async Task DoHttp(Method method, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, resource, action, body, parameters);
        }

        protected async Task<S> DoGet<S>(string resource, string action, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.GET, resource, action, null, parameters);
        }

        protected async Task DoGet(string resource, string action, params QueryParam[] parameters)
        {
            await DoHttp(Method.GET, resource, action, null, parameters);
        }

        protected async Task<S> DoPost<S>(string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.POST, resource, action, body, parameters);
        }

        protected async Task DoPost(string resource, string action, object body, params QueryParam[] parameters)
        {
            await DoHttp(Method.POST, resource, action, body, parameters);
        }

        protected async Task DoDelete(string resource, string action, object body = null, params QueryParam[] parameters)
        {
            await DoHttp(Method.DELETE, resource, action, body == null ? "" : body, parameters);
        }

        protected async Task<S> DoPut<S>(string resource, string action, object body = null, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.PUT, resource, action, body == null ? "" : body, parameters);
        }

        protected async Task DoPut(string resource, string action, object body = null, params QueryParam[] parameters)
        {
            await DoHttp(Method.PUT, resource, action, body == null ? "" : body, parameters);
        }

        protected class MyToken
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
        }

        protected class QueryParam
        {
            public readonly string Name, Value;
            public QueryParam(string name, object value)
            {
                this.Name = name;
                string val = value == null ? "" : value.ToString();
                this.Value = WebUtility.UrlEncode(val);
            }
        }

        #endregion Http Call Helpers

        #region Token Helpers

        protected bool TokenExpired()
        {
            //subtract 30 seconds from expiration date just to be safe
            if (!authToken.tokenExpires.HasValue || DateTime.Now >= authToken.tokenExpires.Value.AddSeconds(-30))
                return true;
            return false;
        }

        public async Task<bool> GetToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BASEURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    HttpContent body = new StringContent("username=" + username + "&password=" + password);
                    body.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    HttpResponseMessage response = await client.PostAsync("token", body);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string json = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                MyToken myToken = JsonConvert.DeserializeObject<MyToken>(json);
                                authToken.token = myToken.access_token;
                                authToken.tokenExpires = DateTime.Now.AddMinutes(myToken.expires_in);
                                return true;
                            }
                            else
                                return false;
                        }
                        else
                        {
                            authToken.token = null;
                            authToken.tokenExpires = null;
                            Exception exc = await HandleBadResponse(response, "token", "");
                            string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                            return false;
                        }
                    }
                    else
                    {
                        authToken.token = null;
                        authToken.tokenExpires = null;
                        Exception exc = await HandleBadResponse(response, "token", "");
                        string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                        return false;
                    }
                }
            }
            catch (Exception exc)
            {
                string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                throw new Exception(message);
            }
        }

        private async Task<Exception> HandleBadResponse(HttpResponseMessage resp, string resource, string action)
        {
            string content = await resp.Content.ReadAsStringAsync();
            return new Exception("Got bad response. HTTP Code:" + resp.StatusCode + ". Error:" + content);
        }

        #endregion Token Helpers

        public async Task<int> AddAlarm(Alarm alarm)
        {
            Int32 key = await DoPost<Int32>("alarm", "", alarm);
            return key;
        }

        public async Task<int> AddSensorReadingHistory(SensorReadingHistory srh)
        {
            Int32 key = await DoPost<Int32>("sensorreadinghistory", "", srh);
            return key;
        }

        public async Task<int> AddSensorStatsHistory(SensorStatsHistory ssh)
        {
            Int32 key = await DoPost<Int32>("sensorstatshistory", "", ssh);
            return key;
        }

        public async Task<List<Alarm>> GetAlarms(int afterMinutes)
        {
            return await DoGet<List<Alarm>>("alarms", afterMinutes.ToString());
        }

        public async Task<QueuedCommand> GetQueuedCommand(int id)
        {
            return await DoGet<QueuedCommand>("queuedcommand", id.ToString());
        }

        public async Task<List<string>> GetGateways(bool? connected)
        {
            if(!connected.HasValue)
                return await DoGet<List<string>>("gateways", "");
            else
                return await DoGet<List<string>>("gateways", connected.ToString());
        }

        public async Task<List<QueuedCommand>> GetQueuedCommands(int count, List<string> gatewayNames)
        {
            var qps = gatewayNames.Select(p => new QueryParam("gatewayNames", p)).ToList();
            qps.Add(new QueryParam("count", count.ToString()));
            return await DoPost<List<QueuedCommand>>("queuedcommands", "Queued", null, qps.ToArray());
        }

        public async Task<List<QueuedCommand>> GetRetryCommands(int count, List<string> gatewayNames)
        {
            var qps = gatewayNames.Select(p => new QueryParam("gatewayNames", p)).ToList();
            qps.Add(new QueryParam("count", count.ToString()));
            return await DoPost<List<QueuedCommand>>("queuedcommands", "Retry", null, qps.ToArray());
        }
        public async Task IncrementQueuedCommandRetry(int id)
        {
            await DoPut("queuedcommand", "IncrementRetry", null, new QueryParam("id", id.ToString()));
        }

        public async Task<int> RunActivate(string tagID)
        {
            return await DoPost<Int32>("queuedcommand", "Activate", null, new QueryParam("tagID", tagID));
        }

        public async Task<int> RunResetTag(string tagID)
        {
            return await DoPost<Int32>("queuedcommand", "ResetTag", null, new QueryParam("tagID", tagID));
        }

        public async Task<int> RunDwellTime(string tagID, int dwellTime)
        {
            return await DoPost<Int32>("queuedcommand", "DwellTime", null, new QueryParam("tagID", tagID), new QueryParam("dwellTime", dwellTime.ToString()));
        }

        public async Task<int> RunGetPosition(string tagID)
        {
            return await DoPost<Int32>("queuedcommand", "GetPosition", null, new QueryParam("tagID", tagID));
        }

        public async Task<int> RunSetAlgorithmType(string tagID, string algorithmType)
        {
            return await DoPost<Int32>("queuedcommand", "SetAlgorithmType", null, new QueryParam("tagID", tagID), new QueryParam("algorithmType", algorithmType));
        }

        public async Task<int> RunSetAccelSensor(string tagID, bool enabled)
        {
            return await DoPost<Int32>("queuedcommand", "SetAccelSensor", null, new QueryParam("tagID", tagID), new QueryParam("enabled", enabled.ToString()));
        }

        public async Task<int> RunSetPosition(string tagID, decimal x, decimal y, int z)
        {
            return await DoPost<Int32>("queuedcommand", "SetPosition", null, new QueryParam("tagID", tagID), new QueryParam("x", x.ToString()), new QueryParam("y", y.ToString()), new QueryParam("z", z.ToString()));
        }

        public async Task<int> RunSetReportingInterval(string gatewayName, string interval)
        {
            return await DoPost<Int32>("queuedcommand", "SetReportingInterval", null, new QueryParam("gatewayName", gatewayName), new QueryParam("interval", interval.ToString()));
        }

        public async Task<int> RunSetSecurityMode(string gatewayName, string securityMode, int thresholdDbm)
        {
            return await DoPost<Int32>("queuedcommand", "SetSecurityMode", null, new QueryParam("gatewayName", gatewayName), new QueryParam("securityMode", securityMode), new QueryParam("thresholdDbm", thresholdDbm.ToString()));
        }

        public async Task<int> RunResetNetwork(string gatewayName)
        {
            return await DoPost<Int32>("queuedcommand", "ResetNetwork", null, new QueryParam("gatewayName", gatewayName));
        }

        public async Task<int> RunSetThreshold(string tagID, string thresholdType, decimal threshold, int duration)
        {
            return await DoPost<Int32>("queuedcommand", "SetThreshold", null, new QueryParam("tagID", tagID), new QueryParam("thresholdType", thresholdType), new QueryParam("threshold", threshold.ToString()), new QueryParam("duration", duration.ToString()));
        }

        public async Task SetGatewayConnectionStatus(string gatewayName, bool connected, string message)
        {
            await DoPut("gateway", "Status", null, new QueryParam("gatewayName", gatewayName), new QueryParam("connected", connected.ToString()), new QueryParam("message", message));
        }

        public async Task SetQueuedCommandStatus(int id, string status, string reason)
        {
            await DoPut("queuedcommand", "Status", null, new QueryParam("id", id.ToString()), new QueryParam("status", status), new QueryParam("reason", reason));
        }

        public async Task<List<SensorReadingHistory>> GetSensorReadingHistories(int withinMinutes)
        {
            return await DoGet<List<SensorReadingHistory>>("sensorreadinghistories", withinMinutes.ToString());
        }

        public async Task<List<SensorStatsHistory>> GetSensorStatsHistories(int withinMinutes)
        {
            return await DoGet<List<SensorStatsHistory>>("sensorstatshistories", withinMinutes.ToString());
        }

        public async Task AddTag(VTag vtag)
        {
            await DoPost("vtag", "", vtag);
        }

        public async Task AddTags(List<VTag> vtags)
        {
            await DoPost("vtags", "", vtags);
        }

        public async Task<VTag> GetTag(string tagID)
        {
            return await DoGet<VTag>("vtag", tagID);
        }

        public async Task<List<VTag>> GetTags()
        {
            return await DoGet<List<VTag>>("vtags", "");
        }

        public async Task<List<VTag>> GetMoved(int minutes)
        {
            return await DoGet<List<VTag>>("vtags", "Moved"+"/"+minutes);
        }

        public async Task DeleteTag(string vtagId)
        {
            await DoDelete("vtag", vtagId, null);
        }
      

        protected class AuthToken
        {
            public string token { get; set; }
            public DateTime? tokenExpires { get; set; }
        }
    }
}
