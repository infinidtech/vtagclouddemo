﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class SensorReadingHistory
    {
        public SensorReadingHistory()
        {
        }       
        public int ID { get; set; }
        public string GatewayName { get; set; }
        public string TagID { get; set; }
        public string SourceDistanceHops { get; set; }
        public int? TTL { get; set; }
        public decimal? MinimumTemperatureDegC { get; set; }
        public decimal? MaximumTemperatureDegC { get; set; }        
        public decimal? MaximumAccelerationG { get; set; }
        public decimal? MaximumLightLevelLux { get; set; }       
        public decimal? BatteryVolts { get; set; }
        public decimal? X { get; set; }
        public decimal? Y { get; set; }
        public int? Z { get; set; }
        public string LocationConfidence { get; set; }
        public DateTime Created { get; set; }
    }
}
