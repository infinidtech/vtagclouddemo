﻿//using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class QueuedCommand
    {
        //[AutoIncrement]
        public int ID { get; set; }
        public string TagID { get; set; }
        public string GatewayName { get; set; }
        public string CommandText { get; set; }
        public string CommandType { get; set; }
        public string Status { get; set; }
        public int RetryCount { get; set; } = 0;
        public DateTime LastAttempt { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public string Reason { get; set; }

        public string PrintOut()
        {
            return "Status:"+Status+", V-Tag ID:" + TagID + ",ReaderID:" + GatewayName + ",CommandType:" + CommandType + ",CommandText:" + CommandText+",Reason="+Reason;
        }
    }
}
