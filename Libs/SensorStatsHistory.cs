﻿using System;
using System.Collections.Generic;
using VTagCloudDemo.Libs;

namespace VTagCloudDemo
{
    public class SensorStatsHistory
    {
        public SensorStatsHistory()
        {
        }              
        public int ID { get; set; }
        public string GatewayName { get; set; }
        public string TagID { get; set; }
        public string SourceDistanceHops { get; set; }
        public int? TTL { get; set; }
        public int? NumNeighbors { get; set; }
        public int? NumActiveNeighbors { get; set; }
        public int? TotalErrors { get; set; }
        public int? LastError { get; set; }
        public string BestNeighbor { get; set; }
        public int? BestNeighborRssiDbm { get; set; }
        public int? MinQueue { get; set; }
        public int? MaxQueue { get; set; }
        public int? AvgQueue { get; set; }
        public int? ProcessedPackets { get; set; }
        public int? AlarmCount { get; set; }
        public int? FirmwareVersion { get; set; }
        public int? MovementTriggers { get; set; }
        public int? SuspectTriggers { get; set; }
        public decimal? DaysSinceReset { get; set; }       
        public int? LeafDroppedPackets { get; set; }
        public DateTime Created { get; set; }
    }
}
