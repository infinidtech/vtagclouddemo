﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class Alarm
    {
        public Alarm() { }
        public Alarm(string gatewayName, string tagId, string thresholdType, string alarmText)
        {
            GatewayName = gatewayName;
            TagId = tagId;
            ThresholdType = thresholdType;
            AlarmText = alarmText;
            Created = DateTime.UtcNow;
        }
        public string ID { get; set; }
        public string GatewayName { get; set; }
        public string TagId { get; set; }
        public string ThresholdType { get; set; }
        public string AlarmText { get; set; }
        public DateTime Created { get; set; }

        public string PrintOut()
        {
            return "V-Tag ID:" + TagId + ", Gateway=" + GatewayName + ", ThresholdType=" + ThresholdType + ", AlarmText=" + AlarmText + ", Created="+Created.ToString("s");
        }
    }
}
