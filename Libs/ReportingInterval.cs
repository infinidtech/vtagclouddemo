﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class ReportingInterval
    {
        public const string Any = "Any";
        public const string FifteenMinutes = "15 Minutes";
        public const string FourHours = "4 Hours";
        public const string OneHour = "1 Hour";
        public const string SixHours = "6 Hours";
        public const string ThirtyMinutes = "30 Minutes";
        public const string TwelveHours = "12 Hours";
        public const string TwentyFourHours = "24 Hours";
        public const string TwoHours = "2 Hours";

        public static List<string> List
        {
            get
            {
                return new List<string>() { Any, FifteenMinutes, ThirtyMinutes, OneHour, TwoHours, FourHours, SixHours, TwelveHours, TwentyFourHours };
            }
        }
    }
}
