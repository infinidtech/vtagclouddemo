﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTagCloudDemo.Libs
{
    public class ClassPrinter
    {
        public static void Printout(object obj)
        {
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(obj);
                Console.Write("{0}={1},", name, value);
            }
            Console.WriteLine();
        }


    }
}
