﻿//using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class VTag 
    {        
        //[PrimaryKey]
        public string TagID { get; set; }
        public string GatewayName { get; set; }
        public string NearestFixed { get; set; }        
        public decimal? X { get; set; }
        public decimal? Y { get; set; }
        public int? Z { get; set; }
        public string PositionType { get; set; }
        public DateTime? LastSeen { get; set; }
        public DateTime? LastMoved { get; set; }
        public bool AccelSensorEnabled { get; set; } = true;
        public string AlgorithmType { get; set; }
        public int? DwellTime { get; set; } = 0;
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public decimal? ThresholdTempLower { get; set; }
        public int? ThresholdTempLowerDuration { get; set; }
        public decimal? ThresholdTempUpper { get; set; }
        public int? ThresholdTempUpperDuration { get; set; }
        public decimal? ThresholdAccelUpper { get; set; }
        public int? ThresholdAccelUpperDuration { get; set; }
        public decimal? ThresholdLuxUpper { get; set; }
        public int? ThresholdLuxUpperDuration { get; set; }
        public decimal? ThresholdBattery { get; set; }
        public int? ThresholdBatteryDuration { get; set; }
      
        public string PrintOut()
        {            
            return "V-Tag ID:" + TagID + ", NearestFixed="+NearestFixed+ ", Gateway="+GatewayName+ ", X:" + X?.ToString("#.##") + ", Y:" + Y?.ToString("#.##") + ", Z:" + Z+", PositionType="+PositionType+", LastSeen="+LastSeen?.ToString("s")+", LastMoved="+LastMoved?.ToString("s");
        }
    }
}
