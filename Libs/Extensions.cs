﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTagCloudDemo.Libs
{
    public static class ExtensionsForStrings
    {
        public static decimal GetDecimal(this string s)
        {
            decimal result;
            bool success = Decimal.TryParse(s, out result);
            if(!success)
                Console.WriteLine("Value is not a number");
            return success ? result : -1;
        }

        public static int GetInt(this string s)
        {
            int result;
            bool success = Int32.TryParse(s, out result);
            if (!success)
                Console.WriteLine("Value is not a integer");
            return success ? result : -1;
        }

        public static bool IsVTag(this string s)
        {
            bool isVTag = s != null && s.Length == 6;
            if (!isVTag)
                Console.WriteLine("Tag ID must be 6 of length");             
            return isVTag;
        }
    }
}
