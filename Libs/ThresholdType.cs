﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTagCloudDemo
{
    public class ThresholdType
    {
        public const string Acceleration_Upper_g = "Acceleration Upper (g)";
        public const string Battery = "Battery";
        public const string Light_Level_Upper_Lux = "Light Level Upper (Lx)";
        public const string Temperature_Lower = "Temperature Lower";
        public const string Temperature_Upper = "Temperature Upper";
        public static List<string> List
        {
            get { return new List<string>() {Acceleration_Upper_g,Battery,Light_Level_Upper_Lux,Temperature_Lower,Temperature_Upper }; }
        }
    }
}
