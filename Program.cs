﻿using System;
using System.Collections.Generic;
using System.Text;
using VTagCloudDemo.Libs;

namespace VTagCloudDemo
{
    class Program
    {
        static CloudClient client;
        static void Main(string[] args)
        {
            Console.WriteLine("Do you want to use the cloud or a local V-Tag Management Service API? ");
            Console.WriteLine("Your choice should reflect what the V-Tag Management Service is configured to do.");
            Console.WriteLine("Please refer to the SDK documentation for more information.");
            Console.WriteLine("Press 1 for cloud or 2 for local:");
            string strCode = Console.ReadLine();
            int code = strCode.GetInt();
            bool useCloud = true;
            string baseUrl = null;
            if (code == 1)
            {
                baseUrl = "https://vtagcloud.infinidtech.com";
            }
            else
            {
                Console.WriteLine("What is the IP Address or hostname of the computer that has the V-Tag Management Service installed?");
                Console.WriteLine("For localhost, just hit enter:");
                string hostname = Console.ReadLine();
                if (String.IsNullOrEmpty(hostname))
                    hostname = "localhost";
                if (hostname.Contains("http://"))
                    hostname = hostname.Replace("http://", "");
                if(hostname.Contains(":"))
                    baseUrl = "http://" + hostname;
                else
                    baseUrl = "http://" + hostname + ":5050";
                useCloud = false;
            }
            string username = "vt@guser", password = "1msc@red0fcl0ud$"; //<-default username and password of the VMS
            if (useCloud)
            {
                Console.WriteLine("Enter cloud username:");
                username = Console.ReadLine();
                Console.WriteLine("Enter cloud password:");
                password = ReadPassword();
            }
            if (username == null || password == null)
            {
                Console.WriteLine("Both login and password must be set.");
                Environment.Exit(-1);
            }
            client = new CloudClient(username, password);
            Console.Clear();
            Console.WriteLine("Referencing " + baseUrl);
            client.SetBaseUrl(baseUrl);
            bool gotToken = false;
            try
            {
                gotToken = client.GetToken().Result;
            }
            catch (Exception exc)
            {
                Console.WriteLine("Got exception when getting security token:" + exc.Message);
                Console.ReadLine();
                Environment.Exit(-1);
            }
            if (!gotToken)
            {
                Console.WriteLine("Unable to login with this username and password.");
                Console.ReadLine();
                Environment.Exit(-1);
            }
            code = 0;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Please choose an option or type q and then hit enter to exit.");
                Console.WriteLine("1) Get Tag");
                Console.WriteLine("2) Get Tags");
                Console.WriteLine("3) Get Moved");

                Console.WriteLine("4) Get SensorReadingHistories");
                Console.WriteLine("5) Get SensorStatHistories");
                Console.WriteLine("6) Get Alarms");
                Console.WriteLine("7) Delete Tag");
                Console.WriteLine("8) Run Activate");
                Console.WriteLine("9) Run Dwell Time");
                Console.WriteLine("10) Run Get Position");
                Console.WriteLine("11) Run Reset Tag");
                Console.WriteLine("12) Run Set Algorithm Type");
                Console.WriteLine("13) Run Reset Network");
                Console.WriteLine("14) Run Set Accel Sensor");
                Console.WriteLine("15) Run Set Fixed Position");
                Console.WriteLine("16) Run Set Reporting Interval");
                Console.WriteLine("17) Run Set Security Mode");
                Console.WriteLine("18) Run Set Threshold");
                Console.WriteLine("19) Get Queued Command Result");
                Console.WriteLine("20) Get Gateways");
                try
                {
                    strCode = Console.ReadLine();
                    if (strCode == "q")
                        Environment.Exit(1);
                    code = strCode.GetInt();
                    switch (code)
                    {
                        case 1:
                            Console.Clear();
                            GetTag();
                            break;
                        case 2:
                            Console.Clear();
                            GetTags();
                            break;
                        case 3:
                            Console.Clear();
                            GetMoved();
                            break;
                        case 4:
                            Console.Clear();
                            GetSensorReadings();
                            break;
                        case 5:
                            Console.Clear();
                            GetSensorStats();
                            break;
                        case 6:
                            Console.Clear();
                            GetAlarms();
                            break;
                        case 7:
                            Console.Clear();
                            DeleteTag();
                            break;
                        case 8:
                            Console.Clear();
                            RunActivate();
                            break;
                        case 9:
                            Console.Clear();
                            RunDwellTime();
                            break;
                        case 10:
                            Console.Clear();
                            RunGetPosition();
                            break;
                        case 11:
                            Console.Clear();
                            RunResetTag();
                            break;
                        case 12:
                            Console.Clear();
                            RunSetAlgorithmType();
                            break;
                        case 13:
                            Console.Clear();
                            RunResetNetwork();
                            break;
                        case 14:
                            Console.Clear();
                            RunSetAccelSensor();
                            break;
                        case 15:
                            Console.Clear();
                            RunSetFixed();
                            break;
                        case 16:
                            Console.Clear();
                            RunSetReportingInterval();
                            break;
                        case 17:
                            Console.Clear();
                            RunSetSecurityMode();
                            break;
                        case 18:
                            Console.Clear();
                            RunSetThreshold();
                            break;
                        case 19:
                            Console.Clear();
                            GetCommand();
                            break;
                        case 20:
                            Console.Clear();
                            GetGateways();
                            break;
                        default:
                            Console.WriteLine("Option not recognized.");
                            break;
                    }
                }
                catch (Exception exc)
                {
                    string innerExc = exc.InnerException?.Message;
                    Console.WriteLine("Got error sending API command:" + exc.Message + innerExc);
                }
                Console.WriteLine("Hit enter to go back to menu");
                Console.ReadLine();
                Console.Clear();
            }
        }

        private static void RunSetThreshold()
        {
            Console.WriteLine("Run Set Threshold: Allows you to set the threshold for a tag sensor.");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            Console.WriteLine("Please choose a threshold type: 1) " + ThresholdType.Acceleration_Upper_g + ", 2) " + ThresholdType.Battery + ", 3) " + ThresholdType.Light_Level_Upper_Lux + ", 4) " + ThresholdType.Temperature_Lower + ", 5) " + ThresholdType.Temperature_Upper);
            int option = Console.ReadLine().GetInt();
            if (option == -1)
                return;
            if (option > ThresholdType.List.Count)
                option = 1;
            decimal level = -1;
            int duration = -1;
            string thresholdType = ThresholdType.List[option - 1];
            Console.WriteLine("You chose threshold type " + thresholdType);
            switch (option)
            {
                case 1:
                    Console.WriteLine("Choose the acceleration level from 0-16 (g) that will trigger the threshold:");
                    level = Console.ReadLine().GetDecimal();
                    break;
                case 2:
                    Console.WriteLine("Choose the battery level from 0-3.5 (v) that will trigger the threshold:");
                    level = Console.ReadLine().GetDecimal();
                    break;
                case 3:
                    Console.WriteLine("Choose the light level from 0-600 (Lux) that will trigger the threshold:");
                    level = Console.ReadLine().GetDecimal();
                    break;
                case 4:
                    Console.WriteLine("Choose the lower temperature (c) that will trigger the threshold:");
                    level = Console.ReadLine().GetDecimal();
                    break;
                case 5:
                    Console.WriteLine("Choose the upper temperature (c) that will trigger the threshold:");
                    level = Console.ReadLine().GetDecimal();
                    break;
            }
            Console.WriteLine("Enter the duration (ms) for the threshold to be triggered:");
            duration = Console.ReadLine().GetInt();
            int cmdId = client.RunSetThreshold(tagid, thresholdType, level, duration).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunSetSecurityMode()
        {
            Console.WriteLine("Run Set Security Mode: Tags that come within range of a gateway in security mode will have their movements reported. This can make a gateway act like a choke point for tags.");
            Console.WriteLine("Enter the gateway you want to run this command under:");
            string gateway = Console.ReadLine();
            Console.WriteLine("Enter the security mode: 1) Off. 2) On");
            int option = Console.ReadLine().GetInt();
            string mode = option == 2 ? "On" : "Off";
            Console.WriteLine("You chose mode " + mode);
            Console.WriteLine("Enter a Threshold Dbm setting from -150 (weakest) to 0 (strongest) signal strength. This is the signal strength required for the gateway to read the tag:");
            int dbm = Console.ReadLine().GetInt();
            if (dbm < -150 || dbm > 0)
                dbm = 0;
            Console.WriteLine("Threshold Dbm was set to " + dbm);
            int cmdId = client.RunSetSecurityMode(gateway, mode, dbm).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunSetReportingInterval()
        {
            Console.WriteLine("Run Set Reporting Interval: Sets the period in which a gateway gives its sensor reports.");
            Console.WriteLine("Enter the gateway you want to run this command under:");
            string gateway = Console.ReadLine();
            var list = ReportingInterval.List;
            Console.WriteLine("Choose an interval option: ");
            Console.Write("1) " + list[0]);
            for (int i = 1; i < list.Count; i++)
            {
                Console.Write(", " + (i + 1) + ") " + list[i]);
            }
            Console.WriteLine();
            int option = Console.ReadLine().GetInt() - 1;
            if (option <= -1)
                return;
            string interval = option < list.Count ? list[option] : ReportingInterval.OneHour;
            Console.WriteLine("You chose interval " + interval);
            int cmdId = client.RunSetReportingInterval(gateway, interval).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunSetAccelSensor()
        {
            Console.WriteLine("Run Set Accel Sensor: Allows you to enable or disable the accelerometer on a tag.");
            Console.WriteLine("Enter the 6 digit tag ID of your fixed tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            Console.WriteLine("Enter 1 for Enabled or 2 for Disabled:");
            int choice = Console.ReadLine().GetInt();
            if (choice != 1 && choice != 2)
            {
                Console.WriteLine("Unrecognized choice. Defaulting to Enabled");
            }
            bool enabled = choice == 2 ? false : true;
            int cmdId = client.RunSetAccelSensor(tagid, enabled).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunResetNetwork()
        {
            Console.WriteLine("Run Reset Network: This will cause every tag in the network to automatically start recalculating its nearest neighbor");
            Console.WriteLine("Enter the gateway you want to run this command under:");
            string gateway = Console.ReadLine();
            int cmdId = client.RunResetNetwork(gateway).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunSetAlgorithmType()
        {
            Console.WriteLine("Run Set Algorithm Type: Setting to none allows you to use a fixed tag as a message relay without asset tag associations");
            Console.WriteLine("Enter the 6 digit tag ID of your fixed tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            Console.WriteLine("Enter 1 for None or 2 for Nearest Fixed:");
            int choice = Console.ReadLine().GetInt();
            if (choice != 1 && choice != 2)
            {
                Console.WriteLine("Unrecognized choice. Defaulting to Nearest Fixed");
            }
            string algorithmType = choice == 1 ? "None" : "Nearest Fixed";
            if (!tagid.IsVTag())
                return;
            int cmdId = client.RunSetAlgorithmType(tagid, algorithmType).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunResetTag()
        {
            Console.WriteLine("Run Reset Tag: Tag starts discovering new neighbors and sending out 1-minute movement reports");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            int cmdId = client.RunResetTag(tagid).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunGetPosition()
        {
            Console.WriteLine("Run Get Position:");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            int cmdId = client.RunGetPosition(tagid).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunDwellTime()
        {
            Console.WriteLine("Run Dwell Time: This command reverses the minute reporting triggered by movement and makes it so it is triggered by a tag holding still for X minutes. Set to 0 to disable.");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            Console.WriteLine("Enter the dwell time (integer):");
            int dwellTime = Console.ReadLine().GetInt();
            if (dwellTime == -1)
                return;
            int cmdId = client.RunDwellTime(tagid, dwellTime).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void RunActivate()
        {
            Console.WriteLine("Activate Tag: This command will cause a tag to start sending geiger-counter messages that the V-Tag Tracker mobile app can utilize to pinpoint a tag down to the last inch.");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            int cmdId = client.RunActivate(tagid).Result;
            Console.WriteLine("Command submitted. Got command id " + cmdId);
        }

        private static void DeleteTag()
        {
            Console.WriteLine("Delete Tag: This command removes the tag from the cloud");
            Console.WriteLine("Enter the 6 digit tag ID of your tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (!tagid.IsVTag())
                return;
            client.DeleteTag(tagid).Wait();
            Console.WriteLine("Tag Deleted.");

        }

        private static void GetAlarms()
        {
            Console.WriteLine("Get Alarms");
            Console.WriteLine("Enter the hours you want to get alarms back to:");
            int hours = Console.ReadLine().GetInt();
            if (hours == -1)
                return;
            if (hours < 1 || hours > 1000)
                hours = 24;
            Console.WriteLine("Getting alarms for the last " + hours + " hours.");
            var values = client.GetAlarms(hours * 60).Result;
            StringBuilder sb = new StringBuilder().AppendLine("Alarms results: count " + values.Count);
            values.ForEach(p => sb.AppendLine(p.PrintOut()));
            Console.WriteLine(sb.ToString());
        }

        private static void GetSensorStats()
        {
            Console.WriteLine("Get SensorStatHistories");
            Console.WriteLine("Within how many minutes? Default is 1440:");
            int minutes = Console.ReadLine().GetInt();
            if (minutes < 1)
            {
                Console.WriteLine("Minutes must be greater than 0. Defaulting to 1440.");
                minutes = 1440;
            }
            List<SensorStatsHistory> srhs = client.GetSensorStatsHistories(minutes).Result;
            Console.WriteLine("Get SensorStats Count-" + srhs.Count);
            srhs.ForEach(p => ClassPrinter.Printout(p));
        }

        private static void GetSensorReadings()
        {
            Console.WriteLine("Get SensorReadingHistories");
            Console.WriteLine("Within how many minutes? Default is 60:");
            int minutes = Console.ReadLine().GetInt();
            if (minutes < 1)
            {
                Console.WriteLine("Minutes must be greater than 0. Defaulting to 60.");
                minutes = 60;
            }
            List<SensorReadingHistory> srhs = client.GetSensorReadingHistories(minutes).Result;
            Console.WriteLine("Get SensorReadings Count-" + srhs.Count);
            srhs.ForEach(p => ClassPrinter.Printout(p));
        }

        private static void RunSetFixed()
        {
            Console.WriteLine("Setting a fixed tag position.");
            Console.WriteLine("Enter the 6 digit tag ID of your fixed tag:");
            string tagid = Console.ReadLine().ToUpper();
            if (tagid.Length != 6)
            {
                Console.WriteLine("Tag ID must be 6 hex characters.");
                return;
            }
            Console.WriteLine("Enter the numeric X coordinate for this fixed tag:");
            decimal x = Console.ReadLine().GetDecimal();
            if (x == -1)
            {
                Console.WriteLine("X must be a number.");
                return;
            }
            Console.WriteLine("Enter the numeric Y coordinate for this fixed tag:");
            decimal y = Console.ReadLine().GetDecimal();
            if (y == -1)
            {
                Console.WriteLine("Y must be a number.");
                return;
            }
            Console.WriteLine("Enter the int Z coordinate for this fixed tag:");
            int z = Console.ReadLine().GetInt();
            if (z == -1)
            {
                Console.WriteLine("Z must be a number.");
                return;
            }
            int cmdId = client.RunSetPosition(tagid, x, y, z).Result;
            Console.WriteLine("Set Position submitted. Got command id " + cmdId);
        }

        private static void GetCommand(int? cmdId = null)
        {
            if (cmdId == null)
            {
                Console.WriteLine("Getting status of command.");
                Console.WriteLine("Enter the command ID you submitted earlier:");
                cmdId = Console.ReadLine().GetInt();
                if (cmdId == -1)
                {
                    Console.WriteLine("Command ID must be an integer.");
                    return;
                }
            }
            QueuedCommand command = client.GetQueuedCommand(cmdId.Value).Result;
            if (command == null)
            {
                Console.WriteLine("Command not found.");
                return;
            }
            else
            {
                Console.WriteLine("Got Command:" + command.PrintOut());
                Console.WriteLine("Type 1 to re-run Get Command");
                string response = Console.ReadLine();
                if (response == "1")
                    GetCommand(cmdId);

            }
        }

        private static void GetMoved()
        {
            Console.WriteLine("Getting tags that have moved in the last X minutes.");
            Console.WriteLine("Enter the number of minutes to check back to:");
            int minutes = Console.ReadLine().GetInt();
            if (minutes == -1)
            {
                Console.WriteLine("minutes value must be an integer.");
                return;
            }
            List<VTag> vtags = client.GetMoved(minutes).Result;
            StringBuilder sb = new StringBuilder().AppendLine("Moved results: count " + vtags.Count);
            vtags.ForEach(p => sb.AppendLine(p.PrintOut()));
            Console.WriteLine(sb.ToString());
        }

        private static void GetTag()
        {
            Console.WriteLine("Get Tag.");
            Console.WriteLine("Enter the Tag ID:");
            string tagId = Console.ReadLine().ToUpper();
            if (!tagId.IsVTag())
                return;
            VTag tag = client.GetTag(tagId).Result;
            if (tag == null)
            {
                Console.WriteLine("No results found.");
                return;
            }
            Console.WriteLine("Got tag: " + tag.PrintOut());
        }

        private static void GetTags()
        {
            Console.WriteLine("Get Tags:");
            Console.WriteLine("Enter 1 to see only V-Tag IDs. Enter 2 to see all related data:");
            int choice = Console.ReadLine().GetInt();
            if (choice != 1 && choice != 2)
            {
                Console.WriteLine("choice must be a 1 or 2.");
                return;
            }
            var tags = client.GetTags().Result;
            if (choice == 1)
            {
                StringBuilder sb = new StringBuilder().AppendLine("GetTags count-" + tags.Count);
                tags.ForEach(p => sb.AppendLine(p.TagID));
                Console.WriteLine(sb.ToString());
            }
            else
            {
                StringBuilder sb = new StringBuilder().AppendLine("GetTags count-" + tags.Count);
                tags.ForEach(p => sb.AppendLine(p.PrintOut()));
                Console.WriteLine(sb.ToString());
            }
        }

        private static void GetGateways()
        {
            Console.WriteLine("Get Gateways:");
            Console.WriteLine("Enter 1 to see online gateways. Enter 2 to see offline gateways:");
            int choice = Console.ReadLine().GetInt();
            if (choice != 1 && choice != 2)
            {
                Console.WriteLine("choice must be a 1 or 2.");
                return;
            }
            bool connected = choice == 1;
            var gateways = client.GetGateways(connected).Result;
            StringBuilder sb = new StringBuilder().AppendLine("Gateways count-" + gateways.Count);
            gateways.ForEach(p => sb.AppendLine(p));
            Console.WriteLine(sb.ToString());
        }

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            // add a new line because user pressed enter at the end of their password
            Console.WriteLine();
            return password;
        }

    }
}