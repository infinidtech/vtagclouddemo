V-Tag Cloud Demo Instructions:

This demo program is a console application which runs on .Net Core. It is compatible with Windows, Macintosh, Linux, as well as Docker containers. 

You can run the app in Visual Studio, Visual Studio Code, or directly from the terminal. Below are instructions for each option:

Running from Terminal:
--------------------------------
Install .Net Core SDK from https://www.microsoft.com/net/core#windowscmd 
   -if you are running from Mac or Linux, see https://www.microsoft.com/net/core for installation instructions
Open your terminal and go to the VTagCloudDemo folder
type "dotnet restore"
type "dotnet run"

Running from Visual Studio:
--------------------------------
Make sure Visual Studio 2017 or later is installed. The free Visual Studio Community edition will work fine.
Open up the VTagCloudDemo.csproj file which will open the project in Visual Studio. Hit F5 to run the program.

Running from Visual Studio Code:
--------------------------------
First download and instal Visual Studio Code from https://code.visualstudio.com/
Install .Net Core SDK from https://www.microsoft.com/net/core#windowscmd
Open the VTagCloudDemo project folder in VS Code
Install Recommended extensions
Open Integrated Terminal under View and type "dotnet restore"
From terminal type "dotnet run"
To Debug, click on the Bug icon on the left. Select .Net Core Attach and press the Green start button. Select your process from the list to attach the debugger.
